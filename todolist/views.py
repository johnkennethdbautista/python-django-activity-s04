from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.forms.models import model_to_dict
from django.utils import timezone
# from django.template import loader

# Local imports
from .models import ToDoItem, Events
from .forms import LoginForm, AddTaskForm, UpdateTaskForm, AddEventForm, UpdateEventForm, Register

# Create your views here.
def index(request):
	# Retrieve a list of all ToDoItem objects from the database
	todoitem_list = ToDoItem.objects.filter(user_id=request.user.id)
	# output = ', '.join([todoitem.task_name for todoitem in todoitem_list])
	# template = loader.get_template("todolist/index.html")
	events_list = Events.objects.filter(user_id=request.user.id)

	# Creates a context dictionary containing data to be passed to the template
	context = { 
		'events_list': events_list,
		'todoitem_list': todoitem_list,
		'user': request.user
	}
	return render(request, "todolist/index.html", context)

def todoitem(request, todoitem_id):
	# response = "You are viewing the details of %s"
	# return HttpResponse(response % todoitem_id)
	# The model_to_dict() allows to convert models into dictionaries
	# get() allows to retrieve a record using it primary key(pk)
	todoitem = get_object_or_404(ToDoItem, pk=todoitem_id)
	return render(request, "todolist/todoitem.html", model_to_dict(todoitem))

def add_task(request):

	context = {}

	if request.method == 'POST':
		form = AddTaskForm(request.POST)

		if form.is_valid() == False:
			form = AddTaskForm()
		else:
			task_name = form.cleaned_data['task_name']
			description = form.cleaned_data['description']

			# Checks if a task already exists in the database
			duplicates = ToDoItem.objects.filter(task_name=task_name)

			if not duplicates:

				ToDoItem.objects.create(task_name=task_name, description=description, date_created=timezone.now(), user_id=request.user.id)
				return redirect("todolist:index")
			
			else:

				context = {
					"error": True
				}

	return render(request, "todolist/add_task.html", context)

def register(request):
	context = {}

	if request.method == 'POST':
		form = Register(request.POST)

		if form.is_valid() == False:
			form = Register()
		else:
			username = form.cleaned_data['username']
			first_name = form.cleaned_data['first_name']
			last_name = form.cleaned_data['last_name']
			email = form.cleaned_data['email']
			password = form.cleaned_data['password']

			users = User.objects.all()
			is_user_registered = False
			context = {
				"is_user_registered": is_user_registered
			}
		
			for indiv_user in users:
				if indiv_user.username == username:
					is_user_registered = True
					break
		
			if is_user_registered == False:
				user = User()
				user.username = username
				user.first_name = first_name
				user.last_name = last_name
				user.email = email
				# set_password() is used to ensure that the password is hashed using Django's authentication framework
				user.set_password(password)
				user.is_staff = False
				user.is_active = True
				user.save()
				context = {
					"first_name" : user.first_name,
					"last_name" : user.last_name
				}
	return render(request, "todolist/register.html", context)


def change_password(request):
	is_user_authenticated = False

	# authenticates the user
	# returns the user object if found and "None" if not found
	user = authenticate(username="johndoe", password="john1234")
	print(user)

	if user is not None:
		authenticated_user = User.objects.get(username="johndoe")
		authenticated_user.set_password("john1")
		authenticated_user.save()
		is_user_authenticated = True

	context = {
		"is_user_authenticated": is_user_authenticated
	}
	return render(request, "todolist/change_password.html", context)


def login_view(request):

	context = {}

	# If this is a POST request we need to process the form data
	if request.method == 'POST':

		# Creates a form instance and populates it with data from the request
		form = LoginForm(request.POST)

		# Check whether the data is valid
		# Runs validation routines for all the form fields and returns True and places the form's data in the "cleaned_data" attribute
		if form.is_valid() == False:

			# Returns a blank login form
			form = LoginForm()

		else:
			# Retrieves the information from the form
			username = form.cleaned_data['username']
			password = form.cleaned_data['password']

			user = authenticate(username=username, password=password)
			context = {
				"username": username,
				"password": password
			}
	
			if user is not None:
				# Saves or creates a record in the django_session table in the database
				login(request, user)
				# redirects the user to the index.html page
				return redirect("todolist:index")

			else:
				# Provides context with error to conditionally render the error message
				context = {
					"error": True
				}

	return render(request, "todolist/login.html", context)

def logout_view(request):
	logout(request)
	return redirect("todolist:index")

def update_task(request, todoitem_id):

	todoitem = ToDoItem.objects.filter(pk=todoitem_id)

	context = {
		"user": request.user,
		"todoitem_id": todoitem_id,
		"task_name": todoitem[0].task_name,
		"description": todoitem[0].description,
		"status" : todoitem[0].status
	}

	if request.method == 'POST':
		form = UpdateTaskForm(request.POST)

		if form.is_valid() == False:

		    form = UpdateTaskForm()
		    
		else:
			task_name = form.cleaned_data['task_name']
			description = form.cleaned_data['description']
			status = form.cleaned_data['status']

			if todoitem:
				todoitem[0].task_name = task_name
				todoitem[0].description = description
				todoitem[0].status = status
				todoitem[0].save()
				return redirect("todolist:index")

			else:

				context = {
					"error": True
				}

	return render(request, "todolist/update_task.html", context)


def delete_task(request, todoitem_id):

	todoitem = ToDoItem.objects.filter(pk=todoitem_id).delete()

	return redirect("todolist:index")

def add_event(request):

	context = {}

	if request.method == 'POST':
		form = AddEventForm(request.POST)

		if form.is_valid() == False:
			form = AddEventForm()
		else:
			event_name = form.cleaned_data['event_name']
			description = form.cleaned_data['description']

			# Checks if a task already exists in the database
			duplicates = Events.objects.filter(event_name=event_name)

			if not duplicates:

				Events.objects.create(event_name=event_name, description=description, user_id=request.user.id)
				return redirect("todolist:index")
			
			else:

				context = {
					"error": True
				}

	return render(request, "todolist/add_event.html", context)

def update_event(request, event_id):

	events = Events.objects.filter(pk=event_id)

	context = {
		"user": request.user,
		"event_id": event_id,
		"event_name": events[0].event_name,
		"description": events[0].description,
		"status" : events[0].status
	}

	if request.method == 'POST':
		form = UpdateEventForm(request.POST)

		if form.is_valid() == False:

		    form = UpdateEventForm()
		    
		else:
			event_name = form.cleaned_data['event_name']
			description = form.cleaned_data['description']
			status = form.cleaned_data['status']

			if events:
				events[0].event_name = event_name
				events[0].description = description
				events[0].status = status
				events[0].save()
				return redirect("todolist:index")

			else:

				context = {
					"error": True
				}

	return render(request, "todolist/update_event.html", context)

def delete_event(request, event_id):

	event = Events.objects.filter(pk=event_id).delete()

	return redirect("todolist:index")

def events(request, event_id):
	# response = "You are viewing the details of %s"
	# return HttpResponse(response % todoitem_id)
	# The model_to_dict() allows to convert models into dictionaries
	# get() allows to retrieve a record using it primary key(pk)
	events = get_object_or_404(Events, pk=event_id)
	return render(request, "todolist/events.html", model_to_dict(events))